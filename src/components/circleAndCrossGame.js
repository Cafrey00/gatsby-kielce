import React, {useEffect, useState} from 'react';
import styled from 'styled-components';

const StyledBoard = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 600px;
    margin: 0 auto;
`;
const StyledField = styled.button`
    background-color: #eee;
    border: 1px solid #000;
    box-shadow: none;
    width: 200px;
    height: 200px;
    outline: none;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 110px;
    font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
`;

const FIELD_VALUES = {
    EMPTY: '',
    CROSS: 'x',
    CIRCLE: 'o',
};

const initialValues = [
    FIELD_VALUES.EMPTY,
    FIELD_VALUES.EMPTY,
    FIELD_VALUES.EMPTY,
    FIELD_VALUES.EMPTY,
    FIELD_VALUES.EMPTY,
    FIELD_VALUES.EMPTY,
    FIELD_VALUES.EMPTY,
    FIELD_VALUES.EMPTY,
    FIELD_VALUES.EMPTY,
];
const winningIndexCombinations = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
];

const CircleAndCrossGame = () => {
    const [fields, setFields] = useState(initialValues);
    const [player, setPlayer] = useState(FIELD_VALUES.CROSS);
    const [result, setResult] = useState(null);

    useEffect(() => {
        // metoda sprawdzajaca czy jakis gracz wygral
        const playerWon = checkWinners();
        // jesli "x" lub "o" to wygral. Jesli "" to nikt nie wygral
        const anyEmptyFieldsLeft = fields.indexOf(FIELD_VALUES.EMPTY) !== -1;
        const isDraw = !playerWon && !anyEmptyFieldsLeft;
        if(playerWon) {
            setResult(playerWon);
        }
        if(isDraw) {
            setResult(FIELD_VALUES.EMPTY);
        }
    }, [fields]);

    const checkWinners = () => {
        // sprawdzenie czy wygral krzyzyk
        const isCrossWinner = checkIfPlayerWon(FIELD_VALUES.CROSS);
        if(isCrossWinner) {
            return FIELD_VALUES.CROSS;
        }
        // sprawdzenie czy wygralo kolko
        const isCircleWinner = checkIfPlayerWon(FIELD_VALUES.CIRCLE);
        if(isCircleWinner) {
            return FIELD_VALUES.CIRCLE;
        }
        // jesli nikt nie wygral to zwracam ""
        return FIELD_VALUES.EMPTY;
    }

    const checkIfPlayerWon = (checkingPlayer) => {
        // sprawdzenie wszystkich kombinacji na bazie winningIndexCombinations
        // tablica w rezultacie ma postac [false, false, false, false, true itd...] 
        const combinationCheckArray = winningIndexCombinations.map((combination) => {
            return checkIfFieldsAreEqualToPlayer(combination, checkingPlayer);
        });
        // jesli true jest w tablicy combinationCheckArray to cala metoda tez zwraca true 
        return combinationCheckArray.indexOf(true) > -1;
    }

    const checkIfFieldsAreEqualToPlayer = (checkingFields, checkingPlayer) => {
        // sprawdzenie wszystkich pol czy maja wartosc = checkingPlayer
        // tablica ma postac [true, true, true]
        const checkedFields = checkingFields.map(fieldIndex => {
            return fields[fieldIndex] === checkingPlayer;
        });
        // Jesli w tablicy nie ma false (indexOf = -1) to cala metoda zwraca true
        return checkedFields.indexOf(false) === -1;
    }
    
    const onClick = (index) => {
        if(fields[index] !== FIELD_VALUES.EMPTY || result !== null) {
            return;
        }
        setFields(fields.map((fieldValue, i) => index === i 
            ? player 
            : fieldValue
        ));
        changePlayer();
    }

    const changePlayer = () => {
        setPlayer(player === FIELD_VALUES.CROSS 
            ? FIELD_VALUES.CIRCLE 
            : FIELD_VALUES.CROSS
        );
    }

    const onNewGame = () => {
        setFields(initialValues);
        setResult(null);
    }

    const showResult = result !== null;

    return (<>
        <h2>Teraz kolej: {player}</h2>
        {showResult && (
            <div>
                <h2>{result === '' ? 'Remis!' : `Wygrał gracz: ${result}`}</h2>
                <button onClick={onNewGame}>Od nowa!</button>
            </div>
        )}
        <StyledBoard>
            {fields.map((fieldValue, index) => (
                <StyledField 
                    key={index}
                    onClick={() => onClick(index)}
                >{fieldValue}</StyledField>
            ))}
        </StyledBoard>
    </>);
}
 
export default CircleAndCrossGame;