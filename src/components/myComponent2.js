import React, { useEffect, useState } from 'react';

export const MyComponent2 = ({title, showAdditionalHeader, text}) => {
    const [iterator, setIterator] = useState(1);
    const [inputValue, setInputValue] = useState('');

    const onButtonClick = () => setIterator(iterator + 1);
    const onChangeInputValue = (event) => setInputValue(event.target.value);

    useEffect(() => {
        console.log('Komponent o tytule ' + title + ' sie zamontowal!');
        return () => {
            console.log('Komponent o tytule ' + title + ' sie odmontowal!');
        }
    }, []);
 
    const onInputValueChange = () => {
        console.log('input value sie zmienil!');
        if(inputValue.length > 10) {
            alert('Przekroczyles 10 znakow!');
        }
    };

    useEffect(onInputValueChange, [inputValue])

    return (<>
        <h1 className="my-header">{title}</h1>
        <h2>Number: {iterator}</h2>
        <h2>Input value: {inputValue}</h2>
        <div>
            {text}
        </div>
        {showAdditionalHeader && (
            <h3>Additional header</h3>
        )}
        <input value={inputValue} onChange={onChangeInputValue} />
        <button onClick={onButtonClick}>Kliknij mnie!</button>
    </>)
}
