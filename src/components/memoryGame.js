import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Button } from '@material-ui/core';

import AnanasIMG from '../images/memory/ananas.jpg';
import ArbuzIMG from '../images/memory/arbuz.jpg';
import BananIMG from '../images/memory/banan.jpg';
import GruszkaIMG from '../images/memory/gruszka.jpg';
import JabloIMG from '../images/memory/jablko.png';
import KiwiIMG from '../images/memory/kiwi.jpeg';
import WinogronaIMG from '../images/memory/winogrona.jpeg';
import WisnieIMG from '../images/memory/wisnie.jpg';

const StyledWrapper = styled.div`
    border: 1px solid red;
    padding: 20px;
    display: flex;
    flex-wrap: wrap;
`;
const StyledCard = styled.button`
    border: 1px solid #000;
    width: 25%;
    background-color: #fff;
    height: 200px;
    outline: none;
    overflow: hidden;

    img {
        margin: 0;
        transition: all 0.5s ease;
        opacity: ${({active}) => active ? '1' : '0'};
        transform: ${({active}) => active 
            ? 'scale(1) rotate(360deg)' 
            : 'scale(0) rotate(0deg)'
        };
        max-width: 100%;
        max-height: 100%;
    }
`;

const imagesDefinitions = [{
    key: 1,
    src: AnanasIMG,
}, {
    key: 2,
    src: ArbuzIMG,
}, {
    key: 3,
    src: BananIMG,
}, {
    key: 4,
    src: GruszkaIMG,
}, {
    key: 5,
    src: JabloIMG,
}, {
    key: 6,
    src: KiwiIMG,
}, {
    key: 7,
    src: WinogronaIMG,
}, {
    key: 8,
    src: WisnieIMG,
}];

const MemoryGame = () => {
    const [moveNumber, setMoveNumber] = useState(1);
    const [images, setImages] = useState([]);
    const [clickedImages, setClickedImages] = useState([]);
    const [guessedKeys, setGuessedKeys] = useState([]);

    useEffect(() => {
        setupImages();
    }, []);

    useEffect(() => {
        if(clickedImages.length !== 2) {
            return;
        }
        const [firstImageIndex, secondImageIndex] = clickedImages;
        const firstImageKey = images[firstImageIndex].key;
        const secondImageKey = images[secondImageIndex].key;
        let timeoutTime = 1500;
        if(firstImageKey === secondImageKey) {
            timeoutTime = 0;
            setGuessedKeys([...guessedKeys, firstImageKey])
        }
        setTimeout(() => {
            setMoveNumber(moveNumber + 1);
            setClickedImages([]);
        }, timeoutTime);
    }, [clickedImages]);

    const setupImages = () => {
        const imagesArray = [...imagesDefinitions, ...imagesDefinitions];
        for(let i = imagesArray.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * i);
            const temp = imagesArray[i];
            imagesArray[i] = imagesArray[j];
            imagesArray[j] = temp;
        }
        setImages(imagesArray);
    }

    const isImageVisible = (index) => {
        const isImageClicked = clickedImages.indexOf(index) > -1;
        const key = images[index].key;
        const isImageGuessed = guessedKeys.indexOf(key) > -1;
        return isImageClicked || isImageGuessed;
    }

    const onClick = (index) => {
        if(isImageVisible(index) || clickedImages.length > 1) {
            return;
        }
        setClickedImages([...clickedImages, index]);
    }

    const onReset = () => {
        setupImages();
        setMoveNumber(1);
        setClickedImages([]);
        setGuessedKeys([]);
    }

    const isResetVisible = guessedKeys.length === imagesDefinitions.length;

    return (<>
        <h1>Memory Game</h1>
        {!isResetVisible && (<h2>Ruch: {moveNumber}</h2>)}
        {isResetVisible && (
            <h3>
                Gra skończona w {moveNumber - 1} ruchach!
                <Button 
                    variant="contained" 
                    color="primary" 
                    onClick={onReset}
                >Uruchom ponownie!</Button>
            </h3>
        )}
        <StyledWrapper>
            {images.map((image, index) => (
                <StyledCard 
                    key={index} 
                    active={isImageVisible(index)}
                    onClick={() => onClick(index)}
                >
                    <img src={image.src} />
                </StyledCard>
            ))}
        </StyledWrapper>
    </>)
}

export default MemoryGame;