import { Link } from 'gatsby';
import React from 'react';  
import { MyComponent2 } from '../components/myComponent2';
import Layout from '../components/layout';
import SEO from '../components/seo';
import Title from '../components/title';

const arrayOfObjects = [{
    id: 1,
    title: 'Title 1',
    text: 'Lorem ipsum dolor sit amet',
    showAdditionalHeader: true,
}, {
    id: 2,
    title: 'Title 2',
    text: 'Lorem ipsum dolor sit amet',
    showAdditionalHeader: false,
}, {
    id: 3,
    title: 'Title 3',
    text: 'Lorem ipsum dolor sit amet',
    showAdditionalHeader: true,
}, {
    id: 4,
    title: 'Title 4',
    text: 'Lorem ipsum dolor sit amet',
    showAdditionalHeader: false,
}, {
    id: 5,
    title: 'Title 5',
    text: 'Lorem ipsum dolor sit amet',
    showAdditionalHeader: true,
}];

class MojaStronaPage extends React.Component {

    componentDidMount() {
    }

    componentWillUnmount() {
        console.log('tu component will unmount!'); 
    }

    render() {
        return (
            <Layout>
                <SEO title="Moja strona!s" />              
                <h1>Moja strona</h1>
                <Link to="/">Wroc na strone glowna</Link>
                <Title 
                    title="tytul 1"
                    subtitle="podtytul 1" 
                />
                {arrayOfObjects.map(
                    (object, key) => (
                        <MyComponent2 key={key} {...object} />
                    )
                )}
                <Title 
                    title="tytul 2adsadsadsadsads"
                    subtitle="podtytul 2adsadsadsdas"
                    changeHeaderColor={true}
                />
                <Title 
                    title="tytul testestestest"
                    subtitle="podtestestest" 
                />
                <Title />
            </Layout>
        );
    }    

}

export default MojaStronaPage;