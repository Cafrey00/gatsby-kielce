import React from 'react';
import { Link } from 'gatsby';
import Layout from '../components/layout';
import SEO from '../components/seo';
import MemoryGame from '../components/memoryGame';

const MemoryPage = () => {
    return (
        <Layout>
            <SEO title="Gra pamięciowa"/>
            <h1>Gra pamięciowa</h1>
            <Link to="/">wróć na stronę główną</Link>
            <MemoryGame />
        </Layout>
    )
}

export default MemoryPage;