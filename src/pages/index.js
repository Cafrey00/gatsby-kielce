import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

// JSX

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Hi peopleeeee</h1>
    <p>Welcome to your new Gatsby site.</p>
    <p>Now go build something great.Jakis moj tekst.</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    <Link to="/moja-strona/">Go to "Moja strona"</Link> <br />
    <Link to="/kolko-i-krzyzyk/">Go to "Kółko i krzyzyk"</Link> <br />
    <Link to="/memory/">Go to "Memory"</Link> <br />
    <Link to="/using-typescript/">Go to "Using TypeScript"</Link>
  </Layout>
)

export default IndexPage
