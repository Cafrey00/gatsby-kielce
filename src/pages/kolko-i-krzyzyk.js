import React from 'react';
import { Link } from 'gatsby';
import Layout from '../components/layout';
import SEO from '../components/seo';
import CircleAndCrossGame from '../components/circleAndCrossGame';

const CircleAndCrossPage = () => {
    return ( 
        <Layout>
            <SEO title="Kółko i krzyzyk"/>
            <h1>Kółko i krzyzyk</h1>
            <Link to="/">wróć na stronę główną</Link>

            <CircleAndCrossGame />

        </Layout>
    );
}
 
export default CircleAndCrossPage;