module.exports = {
  siteMetadata: {
    title: `Gatsby Default Starter`,
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@gatsbyjs`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Gatsby szkolenie Kielce`,
        short_name: `Gatsby Kielce`,
        start_url: `/?source=pwa`,
        background_color: `#339994`,
        theme_color: `#998633`,
        display: `fullscreen`,
        icon: `src/images/memory/jablko.png`, // This path is relative to the root of the site.
      },
    },    
    `gatsby-plugin-offline`,
  ],
}
